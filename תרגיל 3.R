setwd("C:/Users/yerrera/Desktop/?????????? ???????????? ?????????? ??????????")
house.raw <- read.csv("kc_house_data.csv")

str(house.raw)
summary(house.raw)

house.preparpard <- house.raw

#2
isTherNull <- function(df){
  colm<-apply(df,2,function(x) any(is.na(x)))
  return(colm) 
}
isTherNull(house.preparpard)

#turn datetime into character
house.preparpard$date <- as.character(house.preparpard$date)

str(house.preparpard)
#extract weekday
Sys.setlocale("LC_ALL","English")
?Sys.setlocale
date <- substr(house.preparpard$date,1,8)

month <- as.factor(substr(date,5,6))
house.preparpard$month <- month

day <- as.factor(substr(date,7,8))
house.preparpard$day <- day

year <- as.factor(substr(date,1,4))
house.preparpard$year <- year

str(house.preparpard)

#binning
breaks <- c(-1,1000,1980,2000,2024)
labels <- c("not renovated", "old renovation", "renovated", "recent renovation")

#bin the date
bins <- cut(house.preparpard$yr_renovated, breaks = breaks, include.lowest = T, right = F, labels = labels)

summary(bins)

house.preparpard$yr_renovated <- bins

monthsn <- c("January","February","March","April","May","June","July","August","September","October","November","December")
x <- c('01','02','03','04','05','06','07','08','09','10','11','12')
house.preparpard$month <- factor(house.preparpard$month, levels = (x),labels = monthsn)
str(house.preparpard$month)
#install.packages('ggplot2')
library(ggplot2)

breaks <- c(1800,1950,1980,2000,2024)
labels <- c("old house", "orelatively old", "orelatively new", "new house")


bins <- cut(house.preparpard$yr_built, breaks = breaks, include.lowest = T, right = F, labels = labels)

summary(bins)

house.preparpard$yr_built <- bins

ggplot(house.preparpard, aes(month, price)) +geom_boxplot()
ggplot(house.preparpard, aes(yr_built, price)) +geom_boxplot()
ggplot(house.preparpard, aes(yr_renovated, price)) +geom_point() +stat_smooth(method = lm)

install.packages('caTools')
library(caTools)

filter <- sample.split(house.preparpard$price, SplitRatio = 0.7)

str(house.preparpard)
house.preparpard$date <- NULL
house.preparpard$view <- NULL
house.preparpard$id <- NULL
house.preparpard$zipcode <- NULL
house.preparpard$lat <- NULL
house.preparpard$long <- NULL



house.train <- subset(house.preparpard, filter == TRUE)
house.test <- subset(house.preparpard, filter == FALSE)

dim(house.preparpard)
dim(house.train)
dim(house.test)

#run algoritem to get model

model <- lm(price~.,house.train)

summary(model)

predicted.train <- predict(model, house.train)
house.preparpard$date <- NULL
house.train$date <- NULL
house.test$date <- NULL


predicted.test <- predict(model, house.test)

MSE.train <- mean((house.train$price-predicted.train)**2)
#not fun
MSE.test <- mean((house.test$price-predicted.test)**2)

MSE.train**0.5
MSE.test**0.5
mean(house.preparpard$price)
